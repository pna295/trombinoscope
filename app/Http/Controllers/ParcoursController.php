<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Parcour;
use Illuminate\Http\Request;

class ParcoursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $parcours = Parcour::where('classe', 'LIKE', "%$keyword%")
                ->orWhere('annee', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $parcours = Parcour::latest()->paginate($perPage);
        }

        return view('parcours.index', compact('parcours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('parcours.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Parcour::create($requestData);

        return redirect('parcours')->with('flash_message', 'Parcour added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $parcour = Parcour::findOrFail($id);

        return view('parcours.show', compact('parcour'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $parcour = Parcour::findOrFail($id);

        return view('parcours.edit', compact('parcour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $parcour = Parcour::findOrFail($id);
        $parcour->update($requestData);

        return redirect('parcours')->with('flash_message', 'Parcour updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Parcour::destroy($id);

        return redirect('parcours')->with('flash_message', 'Parcour deleted!');
    }
}
