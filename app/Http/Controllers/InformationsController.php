<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InformationsController extends Controller
{
    public function index($id,$name){
        
        return view('informations.index',compact('id','name'));

    }
}
