<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ancien;
use Illuminate\Http\Request;

class AnciensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $anciens = Ancien::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('prenom', 'LIKE', "%$keyword%")
                ->orWhere('date_de_naissance', 'LIKE', "%$keyword%")
                ->orWhere('bp', 'LIKE', "%$keyword%")
                ->orWhere('numero', 'LIKE', "%$keyword%")
                ->orWhere('biographie', 'LIKE', "%$keyword%")
                ->orWhere('mail', 'LIKE', "%$keyword%")
                ->orWhere('localisation', 'LIKE', "%$keyword%")
                ->orWhere('profil_facebook', 'LIKE', "%$keyword%")
                ->orWhere('profil_twitter', 'LIKE', "%$keyword%")
                ->orWhere('profil_linkedIn', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $anciens = Ancien::latest()->paginate($perPage);
        }

        return view('anciens.index', compact('anciens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('anciens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required',
			'prenom' => 'required',
			'date_de_naissance' => 'required',
			'numero' => 'required'
		]);
        $requestData = $request->all();
        
        Ancien::create($requestData);

        return redirect('anciens')->with('flash_message', 'Ancien added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ancien = Ancien::findOrFail($id);

        return view('anciens.show', compact('ancien'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ancien = Ancien::findOrFail($id);

        return view('anciens.edit', compact('ancien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'nom' => 'required',
			'prenom' => 'required',
			'date_de_naissance' => 'required',
			'numero' => 'required'
		]);
        $requestData = $request->all();
        
        $ancien = Ancien::findOrFail($id);
        $ancien->update($requestData);

        return redirect('anciens')->with('flash_message', 'Ancien updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Ancien::destroy($id);

        return redirect('anciens')->with('flash_message', 'Ancien deleted!');
    }

    public function getall(){
        //$anciens = Ancien::orderBy('nom')->get();
        //dd($anciens);
        //return response()->json($anciens, 200, $headers);
    }
}
