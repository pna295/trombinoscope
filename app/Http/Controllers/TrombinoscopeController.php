<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ancien;
use App\Doyen;

class TrombinoscopeController extends Controller{

    public function index(Request $request){
        $anciens = Doyen::orderBy('nom')->get();
        
        return  response()->json($anciens, 200);
        //return view('trombinoscope.index',compact('anciens'));
    }
    public function enreg(Request $request){
        $requestData = $request->all();
        
        Doyen::create($requestData);


    }
}