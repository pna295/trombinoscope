<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Doyen;
use Illuminate\Http\Request;

class DoyensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $doyens = Doyen::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('prenom', 'LIKE', "%$keyword%")
                ->orWhere('adresse_actuelle', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('telephone_1', 'LIKE', "%$keyword%")
                ->orWhere('telephone_2', 'LIKE', "%$keyword%")
                ->orWhere('telephone_3', 'LIKE', "%$keyword%")
                ->orWhere('fonction', 'LIKE', "%$keyword%")
                ->orWhere('biographie', 'LIKE', "%$keyword%")
                ->orWhere('profil_facebook', 'LIKE', "%$keyword%")
                ->orWhere('profil_twitter', 'LIKE', "%$keyword%")
                ->orWhere('profil_linkedIn', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $doyens = Doyen::latest()->paginate($perPage);
        }

        return view('admin.doyens.index', compact('doyens'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.doyens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Doyen::create($requestData);

        return redirect('admin/doyens')->with('flash_message', 'Doyen added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $doyen = Doyen::findOrFail($id);

        return view('admin.doyens.show', compact('doyen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $doyen = Doyen::findOrFail($id);

        return view('admin.doyens.edit', compact('doyen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $doyen = Doyen::findOrFail($id);
        $doyen->update($requestData);

        return redirect('admin/doyens')->with('flash_message', 'Doyen updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Doyen::destroy($id);

        return redirect('admin/doyens')->with('flash_message', 'Doyen deleted!');
    }
}
