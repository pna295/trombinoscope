@extends('layout')
@section('content')
<div class="container my-3">
        <div class="row">
            <div class="col-lg-5">
                <h3>
                

                </h3>
                <!--h5>Description</h5-->
                <p class="lead text-muted">Fiche détaillée de John Doe</p>
            </div>
            <div class="col-lg-7 d-flex justify-content-between my-auto">
                <a class="btn" href="etablissement-3-ces-de-pangar.html" title="CES de Pangar"><span
                        class="lnr lnr-chevron-left"></span></a>
                <a class="btn" href="etablissement-5-lycee-de-ngaoundal.html" title="Lycée de Ngaoundal"><span
                        class="lnr lnr-chevron-right"></span></a>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-5">
                <div class="card mb-4 bg-light">
                    <div class="card-body">
                        <h5 class="card-title">Localisation</h5>
                        <div class="row">
                            <div class="col-12">
                                <span class="small">Région de</span>
                                <div class="lead"><a href="#">Ouest</a></div>
                                <!--<small class="text-muted">105 établissements</small>-->
                            </div>
                        </div>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-lg-6">
                                    <span class="small">Département de</span>
                                    <div class="lead"><a href="dashboard">Noun</a></div>
                                    <!--<small class="text-muted">105 établissements</small>-->
                                </div>

                                <div class="col-lg-6">
                                    <span class="small">Arrondissement de</span>
                                    <div class="lead"><a
                                            href="route('arr',['name' => $item->libelle,'id '=> $item->arrondissement_id])">$item->libelle</a>
                                    </div>
                                    <small class="text-muted">
                                        
                                    </small>
                                </div>
                                <div class="row">
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="card mb-4">
                    <div id="myMap" style="min-height:300px;" class="card-body">
                        <h5 class="card-title">Galerie</h5>
                        <div class="carousel slide" data-ride="carousel" id="carouselExampleIndicators">
                            <ol class="carousel-indicators">
                                <li class="active" data-slide-to="0" data-target="#carouselExampleIndicators"></li>
                                <li data-slide-to="1" data-target="#carouselExampleIndicators"></li>
                                <li data-slide-to="2" data-target="#carouselExampleIndicators"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active"><img alt="First slide" class="d-block w-100"
                                        src="img/monEcran.jpg"></div>
                                <div class="carousel-item"><img alt="Second slide" class="d-block w-100"
                                        src="img/Capture745.jpg"></div>
                                <div class="carousel-item"><img alt="Third slide" class="d-block w-100"
                                        src="img/Books.jpg"></div>
                            </div>
                            <a class="carousel-control-prev" data-slide="prev" href="#carouselExampleIndicators"
                                role="button"><span aria-hidden="true" class="carousel-control-prev-icon"></span> <span
                                    class="sr-only">Previous</span></a>
                            <a class="carousel-control-next" data-slide="next" href="#carouselExampleIndicators"
                                role="button"><span aria-hidden="true" class="carousel-control-next-icon"></span> <span
                                    class="sr-only">Next</span></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Informations principales</h5>
                        <div class="row mt-4">
                            <div class="col-lg-3">
                                <span class="text-muted">N Adhesion</span>
                                <div class="lead">01</div>
                                <!--<span class="small">Classes de form I en form IV</span>-->
                            </div>
                            <div class="col-lg-3">
                                <span class="text-muted">Date Adhésion</span>
                                <div class="lead">2012-10-02</div>
                            </div>
                            <div class="col-lg-3">
                                <span class="text-muted">Fonction</span>
                                <div class="lead">Ingénieur</div>
                            </div>
                            <div class="col-lg-3">
                                    <span class="text-muted">Civilité</span>
                                    <div class="lead">Marié</div>
                                </div>
                        </div>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <h5 class="card-title">Contact</h5>
                            <div class="row mt-4">
                                <div class="col-lg-4">
                                    <span class="text-muted">Boite postale</span>
                                    <div class="lead my-2">$item->bp</div>
                                </div>
                                <div class="col-lg-4">
                                    <span class="text-muted">Adresse email</span>
                                    <div class="lead my-2">$item->email</div>
                                </div>
                                <div class="col-lg-4">
                                    <span class="text-muted">Numéro de téléphone</span>
                                    <div class="lead my-2">$item->telephone</div>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item">
                            <h5 class="card-title">Parcours LYCALBAF</h5>
                            <div class="row mt-4">
                                <div class="col-lg-6">
                                    <span class="small">Classe</span>
                                    
                                </div>
                                <div class="col-lg-6">
                                    <span class="small">Année</span>
                                    
                                </div>
                               <!-- <div class="col-lg-4">
                                    <span class="small">Effectif moyen</span>
                                    <div class="h3">0</div>
                                </div>-->
                            </div>
                            <div class="row mt-4">
                                <div class="col-lg-6">
                                    
                                    <div class="h3">Sixième</div>
                                </div>
                                <div class="col-lg-6">
                                    
                                    <div class="h3">1987-1988</div>
                                </div>
                                <!--<div class="col-lg-4">
                                    <span class="small">Effectif moyen</span>
                     https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css               <div class="h3">0</div>
                                </div>--> 
                            </div>
                        </li>
                        <li class="list-group-item">
                                <h5 class="card-title">Parcours après le LYCALBAF</h5>
                                <div class="row mt-4">
                                    <div class="col-lg-6">
                                        <span class="small">Université</span>
                                        
                                    </div>
                                    <div class="col-lg-6">
                                        <span class="small">Année</span>
                                    </div>
                                    <!--<div class="col-lg-4">
                                        <span class="small">Effectif moyen</span>
                                        <div class="h3">0</div>
                                    </div>-->
                                </div>
                                <div class="row mt-4">
                                        <div class="col-lg-6">
                                            
                                            <div class="h3">Université de Ngoaekelé - Niveau 1</div>
                                        </div>
                                        <div class="col-lg-6">
                                            
                                            <div class="h3">1990-1991</div>
                                        </div>
                                        <!--<div class="col-lg-4">
                                            <span class="small">Effectif moyen</span>
                                            <div class="h3">0</div>
                                        </div>-->
                                    </div>
                            </li>
                        
                    </ul>
                </div>

                <div class="p-3 my-3 jumbotron">
                    <div class="row">
                        <div class="col-6 col-lg-3 text-center text-muted">
                            <span class="text-muted">Facebook</span>
                            <div class="h1 m-3"><span class="fab fa-facebook-f"></span></div>
                            <span class="badge badge-pill badge-secondary"><span class="icon-info"></span> Aucune
                                donnée</span>
                        </div>
                        <div class="col-6 col-lg-3 text-center text-muted">
                            <span class="text-muted">Twitter</span>
                            <div class="h1 m-3"><span class="fab fa-twitter"></span></div>
                            <span class="badge badge-pill badge-secondary"><span class="icon-info"></span> Aucune
                                donnée</span>
                        </div>
                        <div class="col-6 col-lg-3 text-center text-muted">
                            <span class="text-muted">LindedIn</span>
                            <div class="h1 m-3"><span class="fab fa-linkedin-in"></span></div>
                            <span class="badge badge-pill badge-secondary"><span class="icon-info"></span> Aucune
                                donnée</span>
                        </div>
                        <div class="col-6 col-lg-3 text-center text-muted">
                            <div class="text-muted text-truncate">Mail</div>
                            <div class="h1 m-3"><span class="fas fa-envelope"></span></div>
                            <div class="badge d-lg-block badge-pill text-truncate badge-secondary"><span
                                    class="icon-info"></span> Aucune donnée</div>
                        </div>
                    </div>
                </div>
                

            </div>
        </div>
       

        <!--<p class="text-right">
			<a href="modifier-etablissement-4.html" class="btn btn-outline-primary">Modifier</a>
			<a href="ajouter-un-etablissement.html" class="btn">Ajouter un établissement</a>
		</p>-->
    </div>



    
@endsection()