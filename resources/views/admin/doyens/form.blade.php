<div class="form-group{{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', 'Nom', ['class' => 'control-label']) !!}
    {!! Form::text('nom', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('prenom') ? 'has-error' : ''}}">
    {!! Form::label('prenom', 'Prenom', ['class' => 'control-label']) !!}
    {!! Form::text('prenom', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('adresse_actuelle') ? 'has-error' : ''}}">
    {!! Form::label('adresse_actuelle', 'Adresse Actuelle', ['class' => 'control-label']) !!}
    {!! Form::text('adresse_actuelle', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('adresse_actuelle', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
    {!! Form::email('email', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('telephone_1') ? 'has-error' : ''}}">
    {!! Form::label('telephone_1', 'Telephone 1', ['class' => 'control-label']) !!}
    {!! Form::number('telephone_1', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone_1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('telephone_2') ? 'has-error' : ''}}">
    {!! Form::label('telephone_2', 'Telephone 2', ['class' => 'control-label']) !!}
    {!! Form::number('telephone_2', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone_2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('telephone_3') ? 'has-error' : ''}}">
    {!! Form::label('telephone_3', 'Telephone 3', ['class' => 'control-label']) !!}
    {!! Form::number('telephone_3', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone_3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('fonction') ? 'has-error' : ''}}">
    {!! Form::label('fonction', 'Fonction', ['class' => 'control-label']) !!}
    {!! Form::text('fonction', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('fonction', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('biographie') ? 'has-error' : ''}}">
    {!! Form::label('biographie', 'Biographie', ['class' => 'control-label']) !!}
    {!! Form::textarea('biographie', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('biographie', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('profil_facebook') ? 'has-error' : ''}}">
    {!! Form::label('profil_facebook', 'Profil Facebook', ['class' => 'control-label']) !!}
    {!! Form::text('profil_facebook', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('profil_facebook', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('profil_twitter') ? 'has-error' : ''}}">
    {!! Form::label('profil_twitter', 'Profil Twitter', ['class' => 'control-label']) !!}
    {!! Form::text('profil_twitter', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('profil_twitter', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('profil_linkedIn') ? 'has-error' : ''}}">
    {!! Form::label('profil_linkedIn', 'Profil Linkedin', ['class' => 'control-label']) !!}
    {!! Form::text('profil_linkedIn', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('profil_linkedIn', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
