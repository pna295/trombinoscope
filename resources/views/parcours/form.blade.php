<div class="form-group{{ $errors->has('classe') ? 'has-error' : ''}}">
    {!! Form::label('classe', 'Classe', ['class' => 'control-label']) !!}
    {!! Form::text('classe', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('classe', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('annee') ? 'has-error' : ''}}">
    {!! Form::label('annee', 'Annee', ['class' => 'control-label']) !!}
    {!! Form::date('annee', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('annee', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
