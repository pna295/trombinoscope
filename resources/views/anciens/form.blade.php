<div class="form-group{{ $errors->has('nom') ? 'has-error' : ''}}">
    {!! Form::label('nom', 'Nom', ['class' => 'control-label']) !!}
    {!! Form::text('nom', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('prenom') ? 'has-error' : ''}}">
    {!! Form::label('prenom', 'Prenom', ['class' => 'control-label']) !!}
    {!! Form::text('prenom', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('prenom', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_de_naissance') ? 'has-error' : ''}}">
    {!! Form::label('date_de_naissance', 'Date De Naissance', ['class' => 'control-label']) !!}
    {!! Form::date('date_de_naissance', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_de_naissance', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('bp') ? 'has-error' : ''}}">
    {!! Form::label('bp', 'Bp', ['class' => 'control-label']) !!}
    {!! Form::text('bp', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('bp', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('numero') ? 'has-error' : ''}}">
    {!! Form::label('numero', 'Numero', ['class' => 'control-label']) !!}
    {!! Form::number('numero', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('biographie') ? 'has-error' : ''}}">
    {!! Form::label('biographie', 'Biographie', ['class' => 'control-label']) !!}
    {!! Form::textarea('biographie', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('biographie', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('mail') ? 'has-error' : ''}}">
    {!! Form::label('mail', 'Mail', ['class' => 'control-label']) !!}
    {!! Form::email('mail', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('mail', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('localisation') ? 'has-error' : ''}}">
    {!! Form::label('localisation', 'Localisation', ['class' => 'control-label']) !!}
    {!! Form::textarea('localisation', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('localisation', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('profil_facebook') ? 'has-error' : ''}}">
    {!! Form::label('profil_facebook', 'Profil Facebook', ['class' => 'control-label']) !!}
    {!! Form::text('profil_facebook', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('profil_facebook', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('profil_twitter') ? 'has-error' : ''}}">
    {!! Form::label('profil_twitter', 'Profil Twitter', ['class' => 'control-label']) !!}
    {!! Form::text('profil_twitter', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('profil_twitter', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('profil_linkedIn') ? 'has-error' : ''}}">
    {!! Form::label('profil_linkedIn', 'Profil Linkedin', ['class' => 'control-label']) !!}
    {!! Form::text('profil_linkedIn', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('profil_linkedIn', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
