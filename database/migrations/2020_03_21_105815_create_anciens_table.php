<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAnciensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anciens', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->date('date_de_naissance')->nullable();
            $table->string('bp')->nullable();
            $table->integer('numero')->nullable();
            $table->text('biographie')->nullable();
            $table->string('mail')->nullable();
            $table->mediumText('localisation')->nullable();
            $table->string('profil_facebook')->nullable();
            $table->string('profil_twitter')->nullable();
            $table->string('profil_linkedIn')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('anciens');
    }
}
