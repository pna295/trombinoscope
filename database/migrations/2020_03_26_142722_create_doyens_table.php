<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDoyensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doyens', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nom')->nullable();
            $table->string('prenom')->nullable();
            $table->string('adresse_actuelle')->nullable();
            $table->string('email')->nullable();
            $table->integer('telephone_1')->nullable();
            $table->integer('telephone_2')->nullable();
            $table->integer('telephone_3')->nullable();
            $table->string('fonction')->nullable();
            $table->text('biographie')->nullable();
            $table->string('profil_facebook')->nullable();
            $table->string('profil_twitter')->nullable();
            $table->string('profil_linkedIn')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doyens');
    }
}
